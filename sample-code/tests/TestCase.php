<?php

use Illuminate\Support\Facades\DB;

class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    public function setUp()
    {
        DB::beginTransaction();
    }

    public function tearDown()
    {
        DB::rollback();
    }
}
