<?php

use Nakashima\Lead\LeadManager;
use Nakashima\Lead\Exception\LeadException;
use App\Models\Lead;

class LeadManagerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $lead = new Lead();
        $lead->name = "marcelo nakashima";
        $lead->birth = "2000-01-01";
        $lead->region_id = 1;
        $lead->unit_id = 1;
        $lead->email = "marcelo.nakash@gmail.com";
        $lead->phone = "(41) 9567-4665";
        $leadManager = new LeadManager();

        $lead = $leadManager->create($lead);
        $this->assertInstanceOf(Lead::class,$lead);
        $this->assertGreaterThan(0,$lead->id);
    }
}
