<?php

use Nakashima\Lead\Helper\LeadScoreHelper;
use App\Models\Lead;

class LeadScoreHelperTest extends TestCase
{

    public function testCalcAgeScore()
    {
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1999-01-01'), -5); // 17
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1915-01-01'), -5); // 101
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1976-01-01'), -3); // 40
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1966-01-01'), -3); // 50
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1917-01-01'), -3); // 99
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1998-01-01'), 0); // 18
        $this->assertEquals(LeadScoreHelper::calcAgeScore('1977-01-01'), 0); // 39
    }

    public function testCalcRegionUnitScore()
    {
        $test = array(
            'sul' => array('region_id' => 1, 'unit_id' => 1, 'result' => -2),
            'sudeste_sp' => array('region_id' => 2, 'unit_id' => 3, 'result' => 0),// sao paulo
            'sudeste' => array('region_id' => 2, 'unit_id' => 4, 'result' => -1),
            'centrooeste' => array('region_id' => 3, 'unit_id' => 6, 'result' => -3),
            'nordeste' => array('region_id' => 4, 'unit_id' => 7, 'result' => -4),
            'norte' => array('region_id' => 5, 'unit_id' => 8, 'result' => -5)
        );

        foreach ($test as $key => $testInfo) {
            $this->assertEquals(
                LeadScoreHelper::calcRegionUnitScore(
                    $testInfo['region_id'],
                    $testInfo['unit_id']
                ),
                $testInfo['result']
            );
        }
    }


    public function testCalcScore()
    {
        $lead = new Lead();
        $lead->birth = "1998-01-01";
        $lead->region_id = 1;
        $lead->unit_id = 1;

        $this->assertEquals(LeadScoreHelper::calcScore($lead), 8);
        $lead->region_id = 2; // Sudeste
        $lead->unit_id = 3; // São Paulo
        $this->assertEquals(LeadScoreHelper::calcScore($lead), 10);
        $lead->unit_id = 4; // Rio de Janeiro
        $this->assertEquals(LeadScoreHelper::calcScore($lead), 9);
    }
}
