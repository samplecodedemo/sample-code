<?php

use Nakashima\Lead\Helper\LeadValidateHelper;
use App\Models\Lead;

class LeadValidateHelperTest extends TestCase
{

    public function testValidate()
    {
        $lead = new Lead();
        $lead->name = "marcelo nakashima de brito";
        $lead->birth = "1998-01-01";
        $lead->email = "marcelo.nakash@gmail.com";
        $lead->region_id = 1;
        $lead->unit_id = 1;
        $lead->phone = "(41) 9567-4665";
        LeadValidateHelper::validate($lead);
        $this->assertTrue(true);
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 1
     */
    public function testValidateNameSingleWord()
    {
        LeadValidateHelper::name("marcelo");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 2
     */
    public function testValidateNameInvalidNumberCharacter()
    {
        LeadValidateHelper::name("marcelo br1to");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 2
     */
    public function testValidateNameInvalidCharacter()
    {
        LeadValidateHelper::name("marcelo britoç");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 4
     */
    public function testValidateBirth()
    {
        LeadValidateHelper::birth("1990-02-30");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 5
     */
    public function testValidateRegion()
    {
        LeadValidateHelper::region("test");
    }


    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 6
     */
    public function testValidateUnit()
    {
        LeadValidateHelper::unit("test");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 7
     */
    public function testValidateEmail()
    {
        LeadValidateHelper::email("test@test");
    }

    /**
     * @expectedException \Nakashima\Lead\Exception\LeadException
     * @expectedExceptionCode 8
     */
    public function testValidatePhone()
    {
        LeadValidateHelper::phone("41956746665");
    }
}
