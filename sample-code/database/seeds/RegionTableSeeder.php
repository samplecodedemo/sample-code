<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            'id' => 1, 'name' => 'Sul', 'score' => -2
        ]);

        DB::table('regions')->insert([
            'id' => 2, 'name' => 'Sudeste', 'score' => 0
        ]);

        DB::table('regions')->insert([
            'id' => 3, 'name' => 'Centro-Oeste', 'score' => -3
        ]);

        DB::table('regions')->insert([
            'id' => 4, 'name' => 'Nordeste', 'score' => -4
        ]);

        DB::table('regions')->insert([
            'id' => 5, 'name' => 'Norte', 'score' => -5
        ]);
    }
}
