<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            'id' => 1, 'name' => 'Porto Alegre','region_id' => 1, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 2, 'name' => 'Curitiba','region_id' => 1, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 3, 'name' => 'São Paulo','region_id' => 2, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 4, 'name' => 'Rio de Janeiro','region_id' => 2, 'score' => -1
        ]);

        DB::table('units')->insert([
            'id' => 5, 'name' => 'Belo Horizonte','region_id' => 2, 'score' => -1
        ]);

        DB::table('units')->insert([
            'id' => 6, 'name' => 'Brasília','region_id' => 3, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 7, 'name' => 'Salvador','region_id' => 4, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 8, 'name' => 'Recife','region_id' => 4, 'score' => 0
        ]);

        DB::table('units')->insert([
            'id' => 9, 'name' => 'INDISPONÍVEL','region_id' => 5, 'score' => 0
        ]);
    }
}
