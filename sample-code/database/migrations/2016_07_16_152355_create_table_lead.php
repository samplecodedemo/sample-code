<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id',true);
            $table->string('name');
            $table->string('phone');
            $table->date('birth');
            $table->string('email');
            $table->integer('region_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('score');
            $table->timestamps();
        });

        Schema::table('leads', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('unit_id')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
