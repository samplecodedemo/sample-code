<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sample Code</title>
    <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <script src="components/jquery/dist/jquery.min.js"></script>
    <script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="components/jquery.maskedinput/dist/jquery.maskedinput.js"></script>
    <script src="components/moment/min/moment.min.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/app.js"></script>
</head>
<body>

<div class="notify-user alert"></div>
<div class="loading">
    Carregando...
</div>

<div class="container">

    <div class="row">
        <div class="col-lg-12" id="form-container">

            <div class="alert alert-danger form-error" role="alert">
                <p>
                    <b>Atenção!</b> Preencha corretamente os seguintes campos: <br />
                    <span class="form-error-message"></span>
                </p>
            </div>

            <form id="step_1" class="form-step">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Preencha seus dados para receber contato
                        </div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row form-group">
                                <div class="col-lg-6">
                                    <label>Nome Completo</label>
                                    <input class="form-control" type="text" name="nome" placeholder="O seu nome completo">
                                    <div class="form-validation"></div>
                                </div>

                                <div class="col-lg-6">
                                    <label>Data de Nascimento</label>
                                    <input class="form-control" type="text" name="data_nascimento" placeholder="Sua data de nascimento" />
                                    <div class="form-validation"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-lg-6">
                                    <label>Email</label>
                                    <input class="form-control" type="email" name="email" placeholder="email@email.com">
                                    <div class="form-validation"></div>
                                </div>

                                <div class="col-lg-6">
                                    <label>Telefone</label>
                                    <input class="form-control" type="text" name="telefone" placeholder=" (99) 99999-9999">
                                    <div class="form-validation"></div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-lg-6">
                                    <label>Região</label>
                                    <select class="form-control" name="regiao">
                                        <option value="">Selecione a sua região</option>
                                    </select>
                                </div>

                                <div class="col-lg-6">
                                    <label>Unidade</label>
                                    <select class="form-control" name="unidade">
                                        <option value="">Selecione a unidade mais próxima</option>
                                    </select>
                                </div>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-lg btn-info next-step pull-right">Enviar</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </form>

            <div id="step_sucesso" class="form-step" style="display:none">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Obrigado pelo cadastro!
                        </div>
                    </div>
                    <div class="panel-body">
                        Em breve você receberá uma ligação com mais informações!
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
