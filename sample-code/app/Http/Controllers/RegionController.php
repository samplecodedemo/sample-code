<?php

namespace App\Http\Controllers;

use App\Models\Region;

class RegionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function regions()
    {
        return Region::all();
    }
}
