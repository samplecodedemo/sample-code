<?php

namespace App\Http\Controllers;

use Nakashima\Util\Util;
use App\Models\Lead;
use Nakashima\Lead\Exception\LeadException;
use Nakashima\Lead\LeadManager;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Nakashima\Lead\LeadException
     */
    public function create(Request $request)
    {
        $return = array();

        $lead = $this->populateLead($request);
        $leadManager = new LeadManager();

        try {
            $lead = $leadManager->create($lead);
            $return = array('status' => true, "lead" => $lead);
        } catch (LeadException $e) {
            $return = array('status' => false, "message" => $e->getMessage(), "code" => $e->getCode());
        }

        return $return;
    }

    private function populateLead(Request $request)
    {

        $lead = new Lead();
        $lead->name = $request->input("nome");
        $lead->phone = $request->input("telefone");
        $lead->birth = Util::dateFormat($request->input("data_nascimento"));
        $lead->email = $request->input("email");
        $lead->region_id = $request->input("regiao");
        $lead->unit_id = $request->input("unidade");

        return $lead;
    }
}
