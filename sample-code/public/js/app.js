var App = {

    init: function () {
    	this.globalAjaxConfig();
    	this.realTimeValidate();
        this.fieldFilters();
        this.loadRegions();
        this.observerSubmit();
    },

    globalAjaxConfig: function () {
    	$.ajaxSetup({
		    globals: true
		});

		$(document).ajaxStart(function (){
		    Loading.show();
		});

		$(document).ajaxComplete(function (){
		    Loading.hide();
		});

		$(document).ajaxError(function (){
		    Message.error("Erro inesperado, tente novamente mais tarde");
		});
    },

    fieldFilters: function () {

        $('input[name="data_nascimento"]').mask('99/99/9999');       
        $('input[name="telefone"]').mask('(99) 9999-9999?9');

        $('input[name="telefone"]').focusout(function(){
		    var phone, element;
		    element = $(this);
		    element.unmask();
		    phone = element.val().replace(/\D/g, '');
		    if(phone.length > 10) {
		        element.mask("(99) 99999-999?9");
		    } else {
		        element.mask("(99) 9999-9999?9");
		    }
		}).trigger('focusout');


        $('input[name="nome"]').keypress(function (e) {
        	return Validate.onlyLetters(e);
        });
    },

    loadRegions: function () {
        $.ajax({
            url: 'api/regions',
            dataType: 'json',
            success: function (result) {
                $.each(result, function (index, region) {
                    $('select[name="regiao"]').append('<option value="' + region.id + '">' + region.name + '</option>');
                });

                App.regionObserver();
            }
        });
    },

    regionObserver: function () {
        $('select[name="regiao"]').unbind().change(function () {
            var value = $(this).val();
            if (value != '') {
            	App.loadUnits(value);
            } else {
            	$('select[name="unidade"]').html('<option value="">Selecione a unidade mais próxima</option>');
            }
        });
    },

    loadUnits: function (region) {
        $.ajax({
            url: 'api/units/' + region,
            dataType: 'json',
            beforeSend: function () {
            	Loading.show();
            },
            success: function (result) {
                $('select[name="unidade"]').html('<option value="">Selecione a unidade mais próxima</option>');

                $.each(result, function (index, unit) {
                    $('select[name="unidade"]').append('<option value="' + unit.id + '">' + unit.name + '</option>');
                });

                App.regionObserver();
            }
        });
    },

    realTimeValidate: function ()
    {
    	formValidationMessage = function (element,condition,msg) {
    		if(!condition) {
    			$(element).parent().find('.form-validation').html(msg);
    		} else {
    			$(element).parent().find('.form-validation').html('');
    		}
    	};


        $('input[name="nome"]').blur(function () {
        	formValidationMessage(
        		this,
        		$(this).val() != "" && $(this).val().split(' ').length >= 2,
        		"Informe seu nome e sobrenome" 
        	);
        });

		$('input[name="data_nascimento"]').blur(function (){
        	formValidationMessage(
        		this,
        		$(this).val() != "__/__/____" && Validate.date( $(this).val() ),
        		"Informe uma data válida" 
        	);
        });

        $('input[name="email"]').blur(function (){
        	formValidationMessage(
        		this,
        		Validate.email( $(this).val() ),
        		"Informe um email válido!" 
        	);
        });
    },

    validateForm: function () {

        validateForm = {
        	message: [],
        	execute: function (condition,message) {
	        	if (condition) {
	        		validateForm.message.push(message);
	        	}
	        	return this;
	        }
        };


        nome = $('input[name="nome"]').val();
        email = $('input[name="email"]').val();
        nascimento = $('input[name="data_nascimento"]').val(); 
        telefone = $('input[name="telefone"]').val(); 
        regiao = $('select[name="regiao"]').val(); 
        unidade = $('select[name="unidade"]').val(); 

        validateForm.execute(
        	nome == '' || nome.split(' ').length < 2,
        	"- Em nome completo informe seu nome e sobrenome"
        ).execute(
        	nascimento == "",
        	"- Informe uma data de nascimento"
        ).execute(
        	!Validate.email(email),
        	"- Informe um endereço de email válido"
        ).execute(
        	telefone == "",
        	"- Informe telefone para contato"
        ).execute(
        	regiao == "",
        	"- Selecione uma região"
        ).execute(
        	unidade == "",
        	"- Selecione uma unidade"
        );

        if (validateForm.message.length > 0) {
			$('.form-error-message').html(validateForm.message.join("<br />"));
            $('.form-error').show();
			validateForm.message = [];
			return false;
        } else {
        	$('.form-error').hide();
            return true;
        }
    },

    observerSubmit: function () {
        $('.next-step').click(function (event) {
            event.preventDefault();
            var parameters = $('form').serialize();

            if (App.validateForm()) {
                App.submitForm(parameters);
            }

        });
    },

    submitForm: function (parameters) {
        $.ajax({
            url: 'api/lead/create',
            data: parameters,
            type: 'post',
            dataType: 'json',
            success: function (response) {

            	if ( response.status == true ) {
	                $('#step_2').hide();
	                $('#step_sucesso').show();
                } else {
                	Message.error(response.message);
                }
            }
        });
    }

};

$(document).ready(function () {

    App.init();

});