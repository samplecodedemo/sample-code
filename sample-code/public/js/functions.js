var Validate = {
    email: function (email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    onlyLetters: function (e) {
        if (
            e.keyCode > 32 && (e.keyCode < 65 || e.keyCode > 90) &&
            (e.keyCode < 97 || e.keyCode > 122)
        ) {
            return false;
        } else {
            return true;
        }
    },

    date: function (date){

        isValid = false;

        if(date != '') {
                date = date.split('/');
                isValid = moment(date[2] + '-' + date[1] + '-' + date[0]).isValid();  
        }

        return isValid;
    }
}

var Loading = {
    show: function () {
        $('.loading').show();
    },

    hide: function () {
        $('.loading').hide();
    }
}

var Message = {

    message: function (text,type,callback) {

        jQuery('.notify-user').removeClass('alert-danger');
        jQuery('.alert').removeClass('alert-success');
        jQuery('.alert').addClass(type);
        slow = function (){ jQuery('.notify-user').hide('slow',callback);};
        jQuery('.notify-user').html('<i class="fa fa-warning"></i> <strong>Atenção!</strong>&nbsp;&nbsp;'+text);
        jQuery('.notify-user').show('slow',
            function (){  
                setTimeout(slow,3000); 
        });
    },

    success: function (text,callback) {
        if(!callback) callback = function (){};
        Message.message(text,'alert-success',callback);
    },

    error: function (text,callback) {
        if(!callback) callback = function (){};
        Message.message(text,'alert-danger',callback);
    }
}