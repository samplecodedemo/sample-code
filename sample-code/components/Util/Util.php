<?php

namespace Nakashima\Util;

class Util
{
    public static function dateFormat($date)
    {
        $dateArray = explode('/',$date);
        return $dateArray[2] .'-'. $dateArray[1].'-'.$dateArray[0];
    }
}
