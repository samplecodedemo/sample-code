<?php

namespace Nakashima\Lead\Exception;
use \InvalidArgumentException;

/**
 * Class LeadException
 * @package Nakashima\Lead\Exception
 */
class LeadException extends \InvalidArgumentException
{}