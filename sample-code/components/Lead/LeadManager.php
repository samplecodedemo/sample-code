<?php

namespace Nakashima\Lead;

use Nakashima\Lead\Helper\LeadScoreHelper;
use Nakashima\Lead\Helper\LeadValidateHelper;
use Nakashima\Lead\Exception\LeadException;
use App\Models\Lead;

class LeadManager
{
    /**
     * LeadManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * Creates a lead
     *
     * @param Lead $lead
     * @return Lead
     * @throws LeadException
     */
    public function create(Lead $lead)
    {

        LeadValidateHelper::validate($lead);
        $lead->score = LeadScoreHelper::calcScore($lead);
        $lead->save();
        return $lead;
    }
}