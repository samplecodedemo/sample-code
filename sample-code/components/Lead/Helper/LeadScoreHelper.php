<?php

namespace Nakashima\Lead\Helper;

use App\Models\Lead;
use App\Models\Region;
use App\Models\Unit;
use \DateTime;

class LeadScoreHelper
{
    /**
     * Calculates region and unit score
     *
     * @param int $region_id
     * @param int $unit_id
     *
     * @return int
     */
    public static function calcRegionUnitScore($region_id, $unit_id)
    {
        $region = Region::find($region_id);
        $unit = Unit::find($unit_id);

        return $region->score + $unit->score;
    }

    /**
     * Calculates age score
     *
     * @param string $date
     * @return int
     */
    public static function calcAgeScore($date)
    {
        $score = 0;

        $currentDate = new DateTime('2016-06-01');
        $birthDate = new DateTime($date);
        $diff = $currentDate->diff($birthDate);

        $years = $diff->y;

        if ($years < 18 || $years > 100) {
            $score = -5;
        } else if ($years >= 40) {
            $score = -3;
        }
        return $score;
    }

    /**
     * Calculates lead score composed by age and region/unit score
     * Default initial score 10.
     *
     * @param Lead $lead
     * @return int
     */
    public static function calcScore(Lead $lead)
    {
        $initialScore = 10;
        $regionScore = self::calcRegionUnitScore($lead->region_id, $lead->unit_id);
        $ageScore = self::calcAgeScore($lead->birth);

        return $initialScore + $regionScore + $ageScore;
    }
}
