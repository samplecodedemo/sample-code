<?php

    namespace Nakashima\Lead\Helper;

    use Nakashima\Lead\Exception\LeadException;
    use App\Models\Lead;
    use App\Models\Region;
    use App\Models\Unit;
    use \DateTime;

    class leadValidateHelper
    {
        /**
         * Validates a lead instance
         *
         * @param \App\Models\Lead $lead
         *
         * @throws LeadException
         */
        public static function validate(Lead $lead)
        {
            self::name($lead->name);
            self::birth($lead->birth);
            self::email($lead->email);
            self::phone($lead->phone);
            self::region($lead->region_id);
            self::unit($lead->unit_id);
        }

        /**
         * Validates lead's name
         *
         * @param $name
         *
         * @throws LeadException
         */
        public static function name($name)
        {
            $name = explode(" ", $name);
            $nameValidation = ctype_alpha(implode('', $name));
            if (count($name) < 2) {
                throw new LeadException("Nome inválido", 0001);

            } else if (!$nameValidation) {
                throw new LeadException("Nome possui caracteres inválidos", 0002);
            }
        }

        /**
         * Validates lead's birth date
         *
         * @param $birth
         *
         * @throws LeadException
         */
        public static function birth($birth)
        {
            $date = DateTime::createFromFormat('Y-m-d', $birth);

            if (!($date && $date->format('Y-m-d') === $birth)) {
                throw new LeadException("Data de nascimento inválida", 0004);
            }
        }

        /**
         * Validates lead's region
         *
         * @param $region
         *
         * @throws LeadException
         */
        public static function region($region)
        {
            $region = Region::find($region);

            if (empty($region)) {
                throw new LeadException("Região inválida", 0005);
            }
        }

        /**
         * Validates lead's unit
         *
         * @param $unit
         *
         * @throws LeadException
         */
        public static function unit($unit)
        {
            $unit = Unit::find($unit);

            if (empty($unit)) {
                throw new LeadException("Unidate inválida", 0006);
            }
        }

        /**
         * Validates lead's email
         *
         * @param $email
         *
         * @throws LeadException
         */
        public static function email($email)
        {
            $valid = filter_var($email, FILTER_VALIDATE_EMAIL);

            if ($valid === false) {
                throw new LeadException("Email inválido", 0007);
            }
        }

        /**
         * Validates lead's phone number
         *
         * @param $phone
         *
         * @throws LeadException
         */
        public static function phone($phone)
        {
            $match = preg_match("/^\([0-9]{2}\) [0-9]{4,5}-[0-9]{4}$/", $phone);

            if ($match === 0) {
                throw new LeadException("Telefone inválido", 8);
            }
        }
    }
