
After clone the project remane the file .env.example to .env
and set your database configuration

At sample-code root directory run the following commands:
bower install		  # install frontend dependencies
composer install          # install backend dependencies
php artisan migrate       # Creates database tables
php artisan db:seed       # Creates initial tables data ( populates regions and units )


Apache configuration:

<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /{path_do_projeto}/sample-code/public
    ServerAlias sample-code
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

Add to /etc/hosts
127.0.1.1       sample-code

Restart your apache
# service apache2 restart


The system should be available at:
http://sample-code


To run the tests, at sample-code root directory, run the command:
# phpunit
or
# /vendor/phpunit/phpunit/phpunit


The documented endpoints are available at sample-code/postman
